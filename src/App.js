import React from 'react';
import './App.css';
import Right from "./components/Right/Right";
import Left from "./components/Left/Left";

function App() {

    return (
        <div className="App">
            <div id="left">
                <Left />
            </div>
            <div id="right">
                <Right />
            </div>
        </div>
    );
}

export default App;
