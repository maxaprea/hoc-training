import React, {Component} from 'react';
import listaHOC from "../hoc/listaHOC";

const data = {
    left: ['Topolino', 'Pippo', 'Pluto', 'Paperino']
};

function left(props) {

    function eliminaElemento(elemento) {
        props.remove(elemento);
    }

    function stampa(el) {
        props.selezionaElemento(el);
    }

    return (
        <>
            {
                props.data.map((l, i) => <div key={i} className="item">
                    <span onClick={stampa.bind(this, l)}>{l}</span>
                    <button className="" onClick={eliminaElemento.bind(this, l)}>Elimina</button>
                </div>)
            }
        </>
    );
}

const newComponent = listaHOC(left, data.left);

export default newComponent;
