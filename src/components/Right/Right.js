import React, {Component} from 'react';
import listaHOC from "../hoc/listaHOC";

const data = {
    right: {
        nome: 'Massimiliano',
        eta: 36
    }
};

function Right(props) {
    const { nome, eta } = props.data;

    function stampa() {
        props.selezionaElemento(`Ciao, mi chiamo ${nome} e ho ${eta} anni`)
    }

    return (
        <h1 onClick={stampa}>Ciao, mi chiamo {nome} e ho {eta} anni</h1>
    );

}

const newComponent = listaHOC(Right, data.right);

export default newComponent;
