import React, { Component } from 'react';

function listaHOC(WrappedComponent, inputData) {
    return class extends Component{
        constructor(props) {
            super(props);

            this.state = {
                data: inputData
            };

            this.removeItem = this.removeItem.bind(this);
        }

        removeItem(item) {
            const { data } = this.state;
            const idx = data.findIndex(el => el === item);
            data.splice(idx, 1);

            this.setState({
                data: data
            })
        }

        componentDidMount() {
            console.log('Ciao, sono il componente ' + this.constructor.name + ' e sono stato appena creato');
        }

        stampa(el) {
            alert(el)
        }

        render() {
            return <WrappedComponent data={this.state.data}
                                     selezionaElemento={this.stampa}
                                     remove={this.removeItem}
                                     {...this.props} />;
        }

    }
}

export default listaHOC;
